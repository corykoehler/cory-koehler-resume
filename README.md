# Cory Koehler's Resume

Proven ability to build high performance, secure, data driven applications with quality code. Skilled in Microservice architecture and SaaS delivery. Strong understanding of development methodologies such as Agile, and its various branches. Utilizes development methodologies, object-oriented design, test driven development, and best practices to empower organizations and individuals. Known for excellent troubleshooting skills - able to analyze code and engineer well-researched cost-effective and responsive solutions.



## Experiences:
| 9/11/2017 to Current | Software Artisan and Consultant at Pillar Technology - Ann Arbor, MI                                                                                                                                                                    |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Summation                    | Provide guidance, research and teaching at the individual and team level. Help achieve team cohesion to reach high functionality while consulting leadership and delivering high quality tested code in multiple layers of application. |
| Feats               | In only a couple months became the trusted voice for client in cloud readiness.                                                                                                                                                         |
| Technologies        | Azure, C#, Javascript/Angular, CQRS, Event Sourcing     

---

| 4/18/2016 to 9/5/2017 | Software Engineer at Meridian Health Plan - Detroit, MI                                                                                                                                                           |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Summation            | Worked across multiple teams, across multiple layers to provide solutions from the UI-API-Backend.                                                                                                                |
| Feats                | Single handedly provided 1000+ Rest API utilizing existing backend for four front end teams. Lead way in DevOps approach providing framework for the future including scripting of legacy application deployment. |
| Technologies         | Javascript, Layer 7, C#, and Progress                                                                                                                                                                             |

---

| 9/28/2014 to 4/5/2016 | Developer at Nexient (Previously Systems in Motion) - Ann Arbor, MI                                                                                                                              |
|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Summation            | Develop, release and maintain .Net projects in a microservice oriented team. Extreme programming environment with fast paced requirements gathering, solution design, and execution turn around. |
| Feats                | Creation of services oriented to plug in to existing web, database and new designer that communicate to the printers through bucket and batch queueing within 4 months.                          |
| Technologies         | C#, SQL, NServiceBus, MQ, Rest, SOAP                                                                                                                                                             |

---
## Education:
| 9/2009 to 12/2014 | The University of Michigan-Dearborn - Dearborn, MI                      |
|-------------------|-------------------------------------------------------------------------|
| Degree Recieved          | Information Technology Management - Bachelor of Business Administration |

